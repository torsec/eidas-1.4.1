# eidas-1.4.1

This is a mirror for the official eIDAS Node v1.4.1 available on the [CEF Digital] web site.

[CEF Digital]: https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS+Node+version+1.4.1
